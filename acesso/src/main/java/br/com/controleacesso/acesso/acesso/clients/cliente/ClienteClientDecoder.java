package br.com.controleacesso.acesso.acesso.clients.cliente;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 400) {
            return new InvalidClienteException();
        }else{
            return errorDecoder.decode(s, response);
        }

    }
}
