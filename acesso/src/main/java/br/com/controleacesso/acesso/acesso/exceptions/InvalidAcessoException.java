package br.com.controleacesso.acesso.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O acesso informado é inválido!")
public class InvalidAcessoException extends RuntimeException {
}
