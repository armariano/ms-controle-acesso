package br.com.controleacesso.acesso.acesso.clients.cliente;

import br.com.controleacesso.acesso.acesso.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", url = "http://localhost:8080/cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/{id}")
    Cliente findById(@PathVariable Integer id);

}
