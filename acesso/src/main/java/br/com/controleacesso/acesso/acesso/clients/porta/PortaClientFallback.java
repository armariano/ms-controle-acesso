package br.com.controleacesso.acesso.acesso.clients.porta;

import br.com.controleacesso.acesso.acesso.models.Cliente;
import br.com.controleacesso.acesso.acesso.models.Porta;

public class PortaClientFallback implements PortaClient {
    @Override
    public Porta findById(Integer id) {
        return null;
    }
}
