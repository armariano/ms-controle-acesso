package br.com.controleacesso.acesso.acesso.models.dto;

public class AcessoResponse {

    public Integer porta_id;
    public Integer cliente_id;

    public Integer getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Integer porta_id) {
        this.porta_id = porta_id;
    }

    public Integer getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Integer cliente_id) {
        this.cliente_id = cliente_id;
    }
}
