package br.com.controleacesso.acesso.acesso.models.dto;

import br.com.controleacesso.acesso.acesso.models.Acesso;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class AcessoMapper {

    public Acesso toAcesso(AcessoResponse acessoResponse){
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoResponse.getCliente_id());
        acesso.setPortaId(acessoResponse.getPorta_id());
        return acesso;
    }

    public AcessoResponse toAcessoResponse(Acesso acesso){
        AcessoResponse acessoResponse = new AcessoResponse();
        acessoResponse.setCliente_id(acesso.getClienteId());
        acessoResponse.setPorta_id(acesso.getPortaId());
        return acessoResponse;
    }

    public Acesso toAcesso(AcessoRequest acessoRequest){
        Acesso acesso = new Acesso();
        acesso.setClienteId(acessoRequest.getCliente_id());
        acesso.setPortaId(acessoRequest.getPorta_id());
        return acesso;
    }


    public AcessoRequest toAcessoRequest(Acesso acesso){
        AcessoRequest acessoRequest = new AcessoRequest();
        acessoRequest.setCliente_id(acesso.getClienteId());
        acessoRequest.setPorta_id(acesso.getPortaId());
        return acessoRequest;
    }

    public AcessoKafka converterParaAcessoConsumer(AcessoResponse acessoResponse){
        AcessoKafka acessoKafka = new AcessoKafka();
        acessoKafka.setAcessoValido(getRandomBoolean());
        acessoKafka.setCliente_id(acessoResponse.getCliente_id());
        acessoKafka.setPorta_id(acessoResponse.getPorta_id());
        acessoKafka.setDataAtualizacao(LocalDate.now());

        return acessoKafka;
    }

    public static boolean getRandomBoolean() {
        return Math.random() < 0.5;
        // I tried another approaches here, still the same result
    }


}
