package br.com.controleacesso.acesso.acesso.clients.cliente;

import br.com.controleacesso.acesso.acesso.models.Cliente;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente findById(Integer id) {
        Cliente cliente = new Cliente();
        cliente.setId(999);
        cliente.setNome("Cliente Fallback");
        return cliente;

    }
}
