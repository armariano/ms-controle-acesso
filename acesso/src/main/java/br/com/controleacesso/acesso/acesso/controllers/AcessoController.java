package br.com.controleacesso.acesso.acesso.controllers;

import br.com.controleacesso.acesso.acesso.models.Acesso;
import br.com.controleacesso.acesso.acesso.models.dto.AcessoMapper;
import br.com.controleacesso.acesso.acesso.models.dto.AcessoRequest;
import br.com.controleacesso.acesso.acesso.models.dto.AcessoResponse;
import br.com.controleacesso.acesso.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    public ResponseEntity<?> criarAcesso(@RequestBody AcessoRequest acessoRequest) {

        AcessoMapper acessoMapper = new AcessoMapper();
        Acesso acesso = acessoMapper.toAcesso(acessoRequest);

        AcessoResponse acessoResponse = acessoMapper.toAcessoResponse(acessoService.criarAcesso(acesso));

        return ResponseEntity.status(201).body(acessoResponse);

    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public ResponseEntity<?> buscarAcessoPorIds(@PathVariable(name = "cliente_id") int cliente_id, @PathVariable(name = "porta_id") int porta_id) {
        AcessoMapper acessoMapper = new AcessoMapper();
        Acesso acesso = new Acesso();
        acesso.setClienteId(cliente_id);
        acesso.setPortaId(porta_id);

        AcessoResponse acessoResponse = acessoMapper.toAcessoResponse(acessoService.buscarAcesso(acesso));

        acessoService.enviarKafka(acessoMapper.converterParaAcessoConsumer(acessoResponse));

        return ResponseEntity.status(200).body(acessoResponse);
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(code = HttpStatus.OK)
    public void deletarAcesso(@PathVariable(name = "cliente_id") int cliente_id, @PathVariable(name = "porta_id") int porta_id) {
        AcessoMapper acessoMapper = new AcessoMapper();
        Acesso acesso = new Acesso();
        acesso.setClienteId(cliente_id);
        acesso.setPortaId(porta_id);

        acessoService.deletarAcesso(acesso);
    }
}
