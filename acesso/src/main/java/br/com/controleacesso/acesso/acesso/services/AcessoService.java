package br.com.controleacesso.acesso.acesso.services;

import br.com.controleacesso.acesso.acesso.clients.cliente.ClienteClient;
import br.com.controleacesso.acesso.acesso.clients.porta.PortaClient;
import br.com.controleacesso.acesso.acesso.exceptions.InvalidAcessoException;
import br.com.controleacesso.acesso.acesso.models.Acesso;
import br.com.controleacesso.acesso.acesso.models.Cliente;
import br.com.controleacesso.acesso.acesso.models.Porta;
import br.com.controleacesso.acesso.acesso.models.dto.AcessoKafka;
import br.com.controleacesso.acesso.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private KafkaTemplate<String, AcessoKafka> producer;


    public Acesso criarAcesso(Acesso acesso) {

        Cliente cliente = buscarClientePorId(acesso.getClienteId());
        Porta porta = buscarPortaPorId(acesso.getPortaId());

        acesso.setClienteId(cliente.getId());
        acesso.setPortaId(porta.getId());

        return acessoRepository.save(acesso);

    }

    public Acesso buscarAcesso(Acesso acesso) {

        Cliente cliente = buscarClientePorId(acesso.getClienteId());
        Porta porta = buscarPortaPorId(acesso.getPortaId());

        Acesso acesseoRetorno = acessoRepository.findByClienteIdAndPortaId(acesso.getClienteId(), acesso.getPortaId());

        if (acesseoRetorno == null) {
            throw new InvalidAcessoException();
        }

        return acesseoRetorno;
    }

    public void deletarAcesso(Acesso acesso) {
        Cliente cliente = buscarClientePorId(acesso.getClienteId());
        Porta porta = buscarPortaPorId(acesso.getPortaId());
        Acesso acessoRetorno = null;

        acessoRetorno = acessoRepository.findByClienteIdAndPortaId(acesso.getClienteId(), acesso.getPortaId());

        if (acessoRetorno == null) {
            throw new InvalidAcessoException();
        } else {
            acessoRepository.delete(acessoRetorno);
        }
    }

    public Cliente buscarClientePorId(Integer id) {
        return clienteClient.findById(id);
    }

    public Porta buscarPortaPorId(Integer id) {
        return portaClient.findById(id);
    }

    public void enviarKafka(AcessoKafka acessoKafka) {
        producer.send("spec3-alder-rienes-1", acessoKafka);
    }



}
