package br.com.controleacesso.acesso.acesso.clients.porta;

import br.com.controleacesso.acesso.acesso.clients.cliente.InvalidClienteException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 400) {
            return new InvalidPortaException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
