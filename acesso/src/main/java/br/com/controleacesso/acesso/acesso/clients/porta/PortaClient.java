package br.com.controleacesso.acesso.acesso.clients.porta;

import br.com.controleacesso.acesso.acesso.clients.cliente.ClienteClientConfiguration;
import br.com.controleacesso.acesso.acesso.models.Cliente;
import br.com.controleacesso.acesso.acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", url = "http://localhost:8081/porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/{id}")
    Porta findById(@PathVariable Integer id);
}
