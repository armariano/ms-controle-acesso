package br.com.controleacesso.acesso.acesso.repositories;

import br.com.controleacesso.acesso.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    Acesso findByClienteIdAndPortaId(Integer clienteId, Integer portaId);
}
