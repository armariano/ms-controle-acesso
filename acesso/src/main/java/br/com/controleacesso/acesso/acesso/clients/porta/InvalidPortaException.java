package br.com.controleacesso.acesso.acesso.clients.porta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "A porta informada é inválido!")
public class InvalidPortaException extends RuntimeException {
}
