package br.com.controleacesso.porta.porta.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table
public class Porta {

    public Porta() {
    }

    public Porta(int id, @NotNull(message = "andar não pode ser nulo") @NotBlank(message = "andar não pode ser vazio") String andar, @NotNull(message = "sala não pode ser nulo") @NotBlank(message = "sala não pode ser vazio") String sala) {
        this.id = id;
        this.andar = andar;
        this.sala = sala;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "andar não pode ser nulo")
    @NotBlank(message = "andar não pode ser vazio")
    @Column
    private String andar;

    @NotNull(message = "sala não pode ser nulo")
    @NotBlank(message = "sala não pode ser vazio")
    @Column
    private String sala;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }
}
