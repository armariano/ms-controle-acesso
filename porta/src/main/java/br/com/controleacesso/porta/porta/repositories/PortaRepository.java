package br.com.controleacesso.porta.porta.repositories;

import br.com.controleacesso.porta.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
}
