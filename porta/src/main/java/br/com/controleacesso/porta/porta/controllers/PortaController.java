package br.com.controleacesso.porta.porta.controllers;

import br.com.controleacesso.porta.porta.models.Porta;
import br.com.controleacesso.porta.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    public PortaService portaService;

    @PostMapping
    public ResponseEntity<?> criarPorta(@RequestBody Porta porta) {
        try {
            Porta portaRetorno = portaService.criarPorta(porta);
            return ResponseEntity.status(201).body(portaRetorno);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarPortaPorId(@PathVariable(name = "id") int id) {
        try {
            Porta porta = portaService.buscarPortaPorId(id);
            return ResponseEntity.status(200).body(porta);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
