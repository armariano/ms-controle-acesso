package br.com.controleacesso.porta.porta.services;

import br.com.controleacesso.porta.porta.exceptions.PortaNotFoundException;
import br.com.controleacesso.porta.porta.models.Porta;
import br.com.controleacesso.porta.porta.repositories.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta(Porta porta){
        Porta retornoPorta = portaRepository.save(porta);
        return retornoPorta;
    }

    public Porta buscarPortaPorId(int id){
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if(portaOptional.isPresent()){
            return portaOptional.get();
        }
        throw new PortaNotFoundException();
    }
}
