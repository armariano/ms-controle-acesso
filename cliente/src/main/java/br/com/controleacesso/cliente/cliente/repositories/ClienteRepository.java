package br.com.controleacesso.cliente.cliente.repositories;

import br.com.controleacesso.cliente.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
