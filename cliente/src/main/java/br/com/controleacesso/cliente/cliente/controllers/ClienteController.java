package br.com.controleacesso.cliente.cliente.controllers;

import br.com.controleacesso.cliente.cliente.models.Cliente;
import br.com.controleacesso.cliente.cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    public ClienteService clienteService;

    @PostMapping
    public ResponseEntity<?> criarCliente(@RequestBody Cliente cliente) {
        try {
            Cliente clienteRetorno = clienteService.criarCliente(cliente);
            return ResponseEntity.status(201).body(clienteRetorno);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarClientePorId(@PathVariable(name = "id") int id) {
        try {
            Cliente cliente = clienteService.buscarClientePorId(id);
            return ResponseEntity.status(200).body(cliente);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
