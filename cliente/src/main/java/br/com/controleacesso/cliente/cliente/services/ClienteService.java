package br.com.controleacesso.cliente.cliente.services;

import br.com.controleacesso.cliente.cliente.exceptions.ClienteNotFoundException;
import br.com.controleacesso.cliente.cliente.models.Cliente;
import br.com.controleacesso.cliente.cliente.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        Cliente retornoCliente = clienteRepository.save(cliente);
        return retornoCliente;
    }

    @NewSpan("buscaClientePorId")
    public Cliente buscarClientePorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        }
        throw new ClienteNotFoundException();
    }
}
